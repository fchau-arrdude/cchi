var theater = new TheaterJS();

theater
    .describe("Vader", {speed:1, accuracy:1}, "#vader")
    .describe("Luke", 1, "#luke");

theater
    .on("*", function(eventName) {
    })
    .on("say:start, erase:start", function (eventName) {
    })
    .on("say:end, erase:end", function (eventName) {
    });
theater
    .write("Vader:Hi, my name is Jason Wu", 300)
    .write("Luke:I develop, I design, I make cool things.", 400);


(function($) {
    $(window).load(function() {
        $(".nav a").mPageScroll2id({
            scrollSpeed:500
        });
    });

    $(window).scroll(function () {
        if($(window).width() > 800) {

            $('.skill-bar').each(function (i) {

                var bottom_of_object = $(this).position().top + $(this).outerHeight();
                var bottom_of_window = $(window).scrollTop() + $(window).height();

                if (bottom_of_window > bottom_of_object) {

                    $(this).animate({
                        'opacity': '1',
                        'top': '-5em'
                    }, 1000);

                }

            });

            $('#recentworks-wrapper').each(function (i) {

                var bottom_of_object = $(this).position().top + $(this).outerHeight();
                var bottom_of_window = $(window).scrollTop() + $(window).height();

                if (bottom_of_window > bottom_of_object) {

                    $(this).css("opacity", "1");
                    $(this).css("transition", "1s");
                    $('#recentworks').css("margin-left", "0");
                    $('#recentworks').css("transition", "1s");


                }

            });

            $('#aboutme-wrapper').each(function (i) {

                var bottom_of_object = $(this).position().top + $(this).outerHeight();
                var bottom_of_window = $(window).scrollTop() + $(window).height();

                if (bottom_of_window > bottom_of_object) {

                    $(this).css("opacity", "1");
                    $(this).css("transition", "1s");
                    $('#aboutme').css("margin-left", "0");
                    $('#aboutme').css("transition", "1s");


                }

            });

            $('#profile-wrapper').each(function (i) {

                var bottom_of_object = $(this).position().top + $(this).outerHeight();
                var bottom_of_window = $(window).scrollTop() + $(window).height();

                if (bottom_of_window > bottom_of_object) {

                    $('#profile').css("transform", "rotateY(360deg");
                    $(this).css("opacity", "1");
                    $(this).css("transition", "1s");

                }

            });

            $('.currently').each(function (i) {

                var bottom_of_object = $(this).position().top + $(this).outerHeight();
                var bottom_of_window = $(window).scrollTop() + $(window).height();

                if (bottom_of_window > bottom_of_object) {

                    $(this).css("opacity", "1");
                    $(this).css("transition", "1s");
                    $(this).css("margin-left", "0");

                }

            });

            $('.bio-wrap').each(function (i) {

                var bottom_of_object = $(this).position().top + $(this).outerHeight();
                var bottom_of_window = $(window).scrollTop() + $(window).height();

                if (bottom_of_window > bottom_of_object) {

                    $(this).css("opacity", "1");
                    $(this).css("margin-bottom", "0");
                    $(this).css("transition", "1s");

                }

            });

            $('#contact-wrapper').each(function (i) {

                var bottom_of_object = $(this).position().top + $(this).outerHeight();
                var bottom_of_window = $(window).scrollTop() + $(window).height();

                if (bottom_of_window > bottom_of_object) {

                    $(this).css("opacity", "1");
                    $(this).css("transition", "1s");
                    $('#contactme').css("margin-left", "0");
                    $('#contactme').css("transition", "1s");

                }

            });

            $('#email-wrapper').each(function (i) {
                var bottom_of_object = $(this).position().top + $(this).outerHeight();
                var bottom_of_window = $(window).scrollTop() + $(window).height();

                if (bottom_of_window > bottom_of_object) {

                    $(this).css("opacity", "1");
                    $(this).css("transition", "1s");
                    $('#email-line').css("margin-right", "0");
                    $('#email-line').css("transition", "1s");

                }

            });

            $('#social-links-wrapper').each(function (i) {
                var bottom_of_object = $(this).position().top + $(this).outerHeight();
                var bottom_of_window = $(window).scrollTop() + $(window).height();

                if (bottom_of_window > bottom_of_object) {

                    $(this).css("opacity", "1");
                    $(this).css("transition", "1s");
                    $('#social').css("margin-top", "2em");
                    $('#social').css("transition", "1s");

                }

            });

        }
    });
})(jQuery);

(function ($) {
    $(document).ready(function(){

        $('.link').click(function() {

            event.preventDefault();

            newLocation = this.href;

            $('body').fadeOut(200, newpage);

        });

        function newpage() {

            window.location = newLocation;

        }
        $(function () {

            if($(window).width() > 800) {

                $('.navbar').css("background-color", "transparent");
                $('.navbar a').css("color", "#fff");
                $('.navbar').css("border-bottom", "transparent");
                $('.navbar').css("box-shadow", "none");
                $('#brand').attr("src", "img/logo_white.png");

                $(window).scroll(function () {
                    // set distance user needs to scroll before we fadeIn navbar
                    if ($(this).scrollTop() > 50) {
                        $('.navbar').css("background-color", "#fff");
                        $('.navbar a').css("color", "#1b1b1b");
                        $('.navbar').css("box-shadow", "0.1em 0.1em 1em #999");
                        $('.navbar').css("transition", "0.3s");
                        $('#brand').attr("src", "img/logo_black.png");

                    } else {
                        $('.navbar').css("background-color", "transparent");
                        $('.navbar a').css("color", "#fff");
                        $('.navbar').css("border-bottom", "transparent");
                        $('.navbar').css("box-shadow", "none");
                        $('#brand').attr("src", "img/logo_white.png");

                    }
                });
            }
        });

        $('.carousel').carousel({
            interval: 3000
        })

    });
}(jQuery));